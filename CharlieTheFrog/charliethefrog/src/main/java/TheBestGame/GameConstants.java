package TheBestGame;

public class GameConstants {

    public static final String BONUS_FLY_IMAGE = "/image/bonus-fly.png";
    public static final String FROG_IMAGE = "/image/Charlie.png";
    public static final String BAD_FROG_IMAGE = "/image/badFrog.png";
    public static final String SPIT_IMAGE = "/image/spit.png";
    public static final String KNIFE_IMAGE = "/image/knife.png";
    public static final String FLY_IMAGE = "/image/fly.png";
    public static final String WASP_IMAGE = "/image/wasp.png";
    public static final String WASP_LEFT_IMAGE = "/image/waspLeft.png";
    public static final String GAME_OVER_IMAGE = "/image/game-Over.png";
    public static final String GAME_WIN_IMAGE = "/image/happy-ending.png";
    public static final String GRAY_HEART_IMAGE = "/image/gray-heart.png";
    public static final String GROUND_IMAGE = "/image/home.png";
    public static final String INITIAL_SCREEN_IMAGE = "/image/logo.png";
    public static final String LILLY_PAD_IMAGE = "/image/lillyPad.png";
    public static final String POISONOUS_FLY_IMAGE = "/image/poisonous-fly.png";
    public static final String RED_HEART_IMAGE = "/image/red-heart.png";
    public static final String BLACK_HEART_IMAGE = "/image/blackHeart.png";
    public static final String PRINCESS_IMAGE = "/image/rock-w-female.png";
    public static final String WATER_IMAGE = "/image/water.png";
    public static final String WHERE_MY_FLY_IMAGE = "/image/where-my-fly.png";

    public static final String DEATH_SOUND = "/sound/hero-Death.wav";
    public static final String EAT_FLY_SOUND = "/sound/eat-fly.wav";
    public static final String EAT_POISON_SOUND = "/sound/eat-poison.wav";
    public static final String GOLDEN_FLY_SOUND = "/sound/golden-fly.wav";
    public static final String JUMP_SOUND = "/sound/frog-jump.wav";
    public static final String SEX_SOUND = "/sound/sex.wav";
    public static final String WASP_KILL = "/sound/wasp-kill.wav";

    public static final int NUMBER_OF_LIVES = 3;
    public static final int HEARTS_FIRST_X = 15;
    public static final int HEARTS_FIRST_Y = 15;
    public static final int BLACK_FIRST_X = 661;
    public static final int X_DISTANCE_HEARTS = 35;
    public static final int PADDING = 10;
    public static final int GROUND_Y = 560;
    public static final int LILLY_PADS_FIRST_X = 60;
    public static final int LILLY_PADS_FIRST_Y = 160;
    public static final int X_DISTANCE_LILLY_PADS = 100;
    public static final int Y_DISTANCE_LILLY_PADS = 100;
    public static final int X_LIMIT_LILLY_PADS = 695;
    public static final int FROG_INITIAL_X = 360;
    public static final int FROG_MOVE = 100;
    public static final int WASP_MOVE = 50;
    public static final int WASP_Y_POS = 160;
    public static final int WASP_Y_SEC_POS = 260;
    public static final int WASP_Y_NEXT_POS = 200;
    public static final int WASP_X_SEC_POS = 710;
    public static final int SCREEN_WIDTH = 750;
    public static final int SCREEN_HEIGHT = 600;
    public static final int BEGIN_CROC_SECTION = 111;
    public static final int BEGIN_GOAL_SECTION_Y = 20;
    public static final int END_GOAL_SECTION_Y = 100;
    public static final int BEGIN_GOAL_SECTION_X = 340;
    public static final int END_GOAL_SECTION_X = 430;
    public static final int WHERE_MY_FLY_IMAGE_Y = 410;
    public static final int MAX_LILLY_PADS = 28;

}
