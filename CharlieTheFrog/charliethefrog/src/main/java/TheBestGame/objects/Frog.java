package TheBestGame.objects;

import TheBestGame.GameConstants;
import TheBestGame.PlaySound;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import static TheBestGame.GameConstants.*;

public class Frog {

    private boolean alive;
    private boolean win;
    private boolean erased = true;
    private Picture knife;
    private boolean stop = true;

    private int lives;

    private Direction direction;

    private int x;
    private int y;

    private int knifeY;
    private int knifeX;

    private Picture frog;

    public Frog(){

        this.lives = GameConstants.NUMBER_OF_LIVES;

        this.x = GameConstants.FROG_INITIAL_X;
        this.y = GameConstants.GROUND_Y;

        knifeY = y;
        knifeX = x;

        frog = new Picture(x,y, GameConstants.FROG_IMAGE);
        frog.draw();

        alive = true;
        win = false;
    }

    public void knife(){

        stop = false;

        if(erased == true){
            draw();
        }

        knife.translate(0, -WASP_MOVE);
        knifeY -= WASP_MOVE;
        setKnifeY(knifeY);

        if (knifeY == 110) {
            knifeDelete();
            stop = true;
            return;
        }
    }

    public boolean isStop() {
        return stop;
    }

    public void setKnifeY(int y) {
        knifeY = y;
    }

    public void knifeDelete(){
        knife.delete();
        knifeY = y;
        erased = true;
    }

    public void draw(){
        knifeY = y;
        knifeX = x;
        knife = new Picture(knifeX, knifeY, KNIFE_IMAGE);
        knife.draw();
        erased = false;
    }

    public int getKnifeY() {
        return knifeY;
    }

    public int getKnifeX() {
        return knifeX;
    }

    public void hop(){

        switch (direction){

            case UP:
                if(y - GameConstants.FROG_MOVE < GameConstants.PADDING){
                    return;
                }
                frog.translate(0, -GameConstants.FROG_MOVE);
                y -= GameConstants.FROG_MOVE;
                break;

            case DOWN:
                if(y + GameConstants.FROG_MOVE > GameConstants.SCREEN_HEIGHT){
                    return;
                }
                frog.translate(0, GameConstants.FROG_MOVE);
                y += GameConstants.FROG_MOVE;
                break;

            case RIGHT:
                if(x + GameConstants.FROG_MOVE > GameConstants.SCREEN_WIDTH){
                    return;
                }
                frog.translate(GameConstants.FROG_MOVE, 0);
                x += GameConstants.FROG_MOVE;
                break;

            case LEFT:
                if(x - GameConstants.FROG_MOVE < GameConstants.PADDING){
                    return;
                }
                frog.translate(-GameConstants.FROG_MOVE, 0);
                x -= GameConstants.FROG_MOVE;
                break;

            default:
                break;
        }

        PlaySound.jump();

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }

    public boolean isAlive(){
        return alive;
    }

    public int getLives(){
        return lives;
    }

    public void loseLife(){

        lives--;
    }

    public void die(){

        lives = 0;
        alive = false;

        PlaySound.die();
    }

    public void happyEnding() {

        win = true;

        PlaySound.win();
    }


    public boolean isHappy() {
        return win;
    }
}
