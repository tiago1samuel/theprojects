package TheBestGame.objects;

import TheBestGame.GameConstants;
import TheBestGame.PlaySound;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import static TheBestGame.GameConstants.*;

public class BadFrog {

    private boolean alive;
    private int lives;
    private Picture badFrog;
    private Picture spit;
    private boolean erased = true;


    private int x;
    private int y;

    private int frogX;
    private int frogY;

    public BadFrog() {

        this.lives = GameConstants.NUMBER_OF_LIVES;
        this.x = 360;
        this.y = 160;

        frogX = 360;
        frogY = 160;
        alive = true;

        badFrog = new Picture(frogX, frogY, BAD_FROG_IMAGE);
        badFrog.draw();

    }

    public void delete(){
        badFrog.delete();
    }

    public void spit() {
        if(!alive){
            spitDelete();
            return;
        }
        if(erased == true){
            draw();
        }

        if (y == 560) {
            spitDelete();
            return;
        }

        spit.translate(0, WASP_MOVE);
        y += WASP_MOVE;
        setY(y);

    }

    public void setY(int y) {
        this.y = y;
    }

    public void spitDelete(){
        spit.delete();
        y = 160;
        erased = true;
    }

    public void draw(){
        spit = new Picture(x, y, SPIT_IMAGE);
        spit.draw();
        erased = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void loseLife(){

        lives--;
    }

    public void die(){

        lives = 0;
        alive = false;
        badFrog.delete();

    }

    public int getLives() {
        return lives;
    }

    public int getFrogX() {
        return frogX;
    }

    public int getFrogY() {
        return frogY;
    }

    public boolean isAlive() {
        return alive;
    }
}
