package TheBestGame.objects;

import TheBestGame.GameConstants;
import TheBestGame.objects.flies.BonusFly;
import TheBestGame.objects.flies.Fly;
import TheBestGame.objects.flies.PoisonFly;
import TheBestGame.objects.screen.Goal;
import TheBestGame.objects.screen.LillyPad;

abstract public class GameObjectFactory {

    public static Goal createGoal(){

        return new Goal();
    }

    public static LillyPad createLillyPad(int x, int y){

        return new LillyPad(x, y);

    }

    public static Fly createFly(int x, int y){

        return new Fly(x,y, GameConstants.FLY_IMAGE);
    }

    public static BonusFly createBonusFly(int x, int y){

        return new BonusFly(x, y, GameConstants.BONUS_FLY_IMAGE);
    }

    public static PoisonFly createPoisonFly(int x, int y){

        return new PoisonFly(x, y, GameConstants.POISONOUS_FLY_IMAGE);
    }

    public static Wasp createWasp(int x, int y, Direction direction, String fileName){

        return new Wasp(x,y, direction, fileName);
    }
}
