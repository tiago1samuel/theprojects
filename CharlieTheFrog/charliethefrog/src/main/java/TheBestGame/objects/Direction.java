package TheBestGame.objects;

import TheBestGame.GameConstants;

public enum Direction {

    UP(0, -GameConstants.FROG_MOVE),
    DOWN(0, GameConstants.FROG_MOVE),
    LEFT(-GameConstants.FROG_MOVE, 0),
    RIGHT(GameConstants.FROG_MOVE, 0);

    private int deltaX;
    private int deltaY;

    Direction(int deltaX, int deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    public int getDeltaX() {
        return deltaX;
    }

    public int getDeltaY() {
        return deltaY;
    }
}
