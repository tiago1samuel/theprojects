package TheBestGame.objects.flies;

import TheBestGame.PlaySound;


public class PoisonFly extends Fly {


    public PoisonFly(int x, int y, String picturePath) {
        super(x, y, picturePath);
        setX(x);
        setY(y);

    }

    @Override
    public void eatSound() {
        PlaySound.eatPoisonousFly();
    }
}
