package TheBestGame.objects.flies;

import TheBestGame.PlaySound;
import TheBestGame.objects.GameObject;

public class Fly extends GameObject {

    private boolean alive;

    public Fly(int x, int y, String picturePath) {

        super(x, y, picturePath);
        alive = true;
        setX(x);
        setY(y);

    }

    public boolean isAlive(){
        return alive;
    }

    public void die(){

        alive = false;
        delete();
        eatSound();
    }

    public void eatSound() {

        PlaySound.eatFly();
    }

}
