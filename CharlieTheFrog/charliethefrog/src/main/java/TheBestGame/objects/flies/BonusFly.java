package TheBestGame.objects.flies;

import TheBestGame.PlaySound;

public class BonusFly extends Fly {


    public BonusFly(int x, int y, String picturePath) {
        super(x, y, picturePath);
        setX(x);
        setY(y);

    }

    @Override
    public void eatSound(){
        PlaySound.eatGoldenFly();
    }

}
