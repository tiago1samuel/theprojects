package TheBestGame.objects;

import TheBestGame.GameConstants;
import TheBestGame.PlaySound;

import static TheBestGame.GameConstants.PADDING;
import static TheBestGame.GameConstants.WASP_X_SEC_POS;

public class Wasp extends GameObject{

    private int x;
    private Direction direction;

    Wasp(int x, int y, Direction direction, String fileName){

        super(x, y, fileName);
        this.x = x;

        setX(x);
        setY(y);

        this.direction = direction;

    }

    public void waspSound(){
        PlaySound.waspHit();
    }

    public void fly(){
        if(direction == Direction.RIGHT){

            if(x == WASP_X_SEC_POS){
                direction = Direction.LEFT;
                return;
            }

            picture.translate(GameConstants.WASP_MOVE, 0);
            x += GameConstants.WASP_MOVE;
            setX(x);

        }
        if (direction == Direction.LEFT) {
            if(x == PADDING){
                direction = Direction.RIGHT;
                return;
            }

            picture.translate(-GameConstants.WASP_MOVE, 0);
            x -= GameConstants.WASP_MOVE;
            setX(x);

        }
    }
}
