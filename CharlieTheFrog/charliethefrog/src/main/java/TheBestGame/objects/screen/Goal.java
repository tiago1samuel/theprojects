package TheBestGame.objects.screen;

import TheBestGame.GameConstants;
import TheBestGame.objects.GameObject;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Goal extends GameObject {

    private Picture whereMyFly = new Picture(GameConstants.PADDING, GameConstants.WHERE_MY_FLY_IMAGE_Y, GameConstants.WHERE_MY_FLY_IMAGE);

    public Goal() {

        super(GameConstants.BEGIN_GOAL_SECTION_X, GameConstants.BEGIN_GOAL_SECTION_Y, GameConstants.PRINCESS_IMAGE);

    }

    public void noJoy() {

        whereMyFly.draw();
    }

    public void deleteMessage() {

        whereMyFly.delete();
    }

}
