package TheBestGame.objects.screen;

import TheBestGame.GameConstants;
import TheBestGame.objects.GameObject;

public class LillyPad extends GameObject {

    public LillyPad(int x, int y) {

        super(x, y, GameConstants.LILLY_PAD_IMAGE);
        setX(x);
        setY(y);

    }
}