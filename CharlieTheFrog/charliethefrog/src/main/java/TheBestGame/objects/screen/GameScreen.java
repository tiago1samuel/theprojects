package TheBestGame.objects.screen;

import TheBestGame.GameConstants;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class GameScreen {

    private Picture screen;

    public GameScreen() {

        screen = new Picture(GameConstants.PADDING, GameConstants.PADDING, GameConstants.INITIAL_SCREEN_IMAGE);
        screen.draw();
    }

    public void pond(){

        Picture pond = new Picture(GameConstants.PADDING, GameConstants.PADDING, GameConstants.WATER_IMAGE);
        pond.draw();
    }

    public void home(){

        Picture home = new Picture(GameConstants.PADDING, GameConstants.GROUND_Y, GameConstants.GROUND_IMAGE);
        home.draw();
    }

    public void deleteScreen(){
        screen.delete();
    }
}
