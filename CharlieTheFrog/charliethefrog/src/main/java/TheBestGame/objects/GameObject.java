package TheBestGame.objects;

import org.academiadecodigo.simplegraphics.pictures.Picture;

abstract public class GameObject {

    private int x;
    private int y;
    protected Picture picture;

    public GameObject(int x, int y, String picturePath) {
        this.picture = new Picture(x, y, picturePath);
        this.picture.draw();
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void draw() {
        picture.draw();
    }

    public void delete() {
        picture.delete();
    }
}

