package TheBestGame;

public class PlaySound {

    public static void jump() {
        Sound jumpSound = new Sound(GameConstants.JUMP_SOUND);
        jumpSound.play(true);
    }

    public static void die() {
        Sound deathSound = new Sound(GameConstants.DEATH_SOUND);
        deathSound.play(true);
    }

    public static void win() {
        Sound winSound = new Sound(GameConstants.SEX_SOUND);
        winSound.play(true);
    }

    public static void eatGoldenFly() {
        Sound goldenFly = new Sound(GameConstants.GOLDEN_FLY_SOUND);
        goldenFly.play(true);
    }

    public static void eatFly() {
        Sound fly = new Sound(GameConstants.EAT_FLY_SOUND);
        fly.play(true);
    }

    public static void eatPoisonousFly() {
        Sound poisonousFly = new Sound(GameConstants.EAT_POISON_SOUND);
        poisonousFly.play(true);
    }

    public static void waspHit() {
        Sound waspHit = new Sound(GameConstants.WASP_KILL);
        waspHit.play(true);
    }

}
