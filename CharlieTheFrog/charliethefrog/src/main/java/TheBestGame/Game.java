package TheBestGame;

import TheBestGame.objects.*;
import TheBestGame.objects.flies.BonusFly;
import TheBestGame.objects.flies.Fly;
import TheBestGame.objects.flies.PoisonFly;
import TheBestGame.objects.screen.GameScreen;
import TheBestGame.objects.screen.Goal;
import TheBestGame.objects.screen.LillyPad;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.util.LinkedList;

public class Game implements KeyboardHandler {

    private Frog charlie;
    private Goal goal;
    private LillyPad[] lillyPads;
    private LinkedList<Fly> flies;
    private BonusFly bonusFly;
    private Wasp[] wasps;
    private GameScreen gameScreen;
    private BadFrog badFrog;
    private boolean bonusFlyEaten = false;

    private Keyboard keyboard;

    private boolean isStarted;
    private boolean isGameOver;

    private Picture gameOver;
    private Picture happyEnding;
    private Picture[] redHeart;
    private Picture[] blackHeart;

    private int delay;
    private int flyCounter;


    Game() {

        isStarted = false;
        isGameOver = true;
        this.delay = 100;
        keyboard = new Keyboard(this);
    }

    void init() throws InterruptedException{

        gameScreen = new GameScreen();

        KeyboardEvent start = new KeyboardEvent();
        start.setKey(KeyboardEvent.KEY_SPACE);
        start.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent a = new KeyboardEvent();
        a.setKey(KeyboardEvent.KEY_A);
        a.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        keyboard.addEventListener(start);
        keyboard.addEventListener(left);
        keyboard.addEventListener(right);
        keyboard.addEventListener(up);
        keyboard.addEventListener(down);
        keyboard.addEventListener(a);

        while ((isGameOver)){

            System.out.println("Press Space");
        }

        start();
    }

    private void start() throws InterruptedException{

        isStarted = true;
        isGameOver = false;
        gameScreen.deleteScreen();
        gameScreen.pond();
        gameScreen.home();

        flyCounter = 0;

        createGoal();
        createLilliPads();
        createHearts();
        charlie = new Frog();
        createWasp();

        while (true) {

            Thread.sleep(delay);

            moveAllWasp();
            checkFlies();
            checkPos();
            if(bonusFlyEaten){
                badFrog.spit();
            }
            if(!charlie.isStop()){
                charlie.knife();
            }
        }
    }

    private void createLilliPads() {

        int x = GameConstants.LILLY_PADS_FIRST_X;
        int y = GameConstants.LILLY_PADS_FIRST_Y;

        lillyPads = new LillyPad[GameConstants.MAX_LILLY_PADS];
        flies = new LinkedList<>();

        for (int i = 0; i < lillyPads.length; i++) {

            if (x > GameConstants.X_LIMIT_LILLY_PADS) {

                x = GameConstants.LILLY_PADS_FIRST_X;
                y += GameConstants.Y_DISTANCE_LILLY_PADS;
            }

            if (Math.random() < 0.6) {

                lillyPads[i] = GameObjectFactory.createLillyPad(x, y);

            } else {

                if (Math.random() <= 0.5) {

                    lillyPads[i] = GameObjectFactory.createLillyPad(x, y);
                    flies.add(GameObjectFactory.createFly(x, y));
                    flyCounter++;

                } else {

                    lillyPads[i] = GameObjectFactory.createLillyPad(x, y);
                    flies.add(GameObjectFactory.createPoisonFly(x, y));
                }
            }

            x += GameConstants.X_DISTANCE_LILLY_PADS;
        }
    }

    private void createHearts() {

        redHeart = new Picture[GameConstants.NUMBER_OF_LIVES];
        Picture[] grayHeart = new Picture[GameConstants.NUMBER_OF_LIVES];

        int x = GameConstants.HEARTS_FIRST_X;
        int y = GameConstants.HEARTS_FIRST_Y;

        for (int i = 0; i < redHeart.length; i++) {

            grayHeart[i] = new Picture(x, y, GameConstants.GRAY_HEART_IMAGE);
            grayHeart[i].draw();

            redHeart[i] = new Picture(x, y, GameConstants.RED_HEART_IMAGE);
            redHeart[i].draw();

            x += GameConstants.X_DISTANCE_HEARTS;
        }
    }

    private void createBlackHearts() {

        blackHeart = new Picture[GameConstants.NUMBER_OF_LIVES];
        Picture[] grayHeart = new Picture[GameConstants.NUMBER_OF_LIVES];

        int x = GameConstants.BLACK_FIRST_X;
        int y = GameConstants.HEARTS_FIRST_Y;

        for (int i = 0; i < redHeart.length; i++) {

            grayHeart[i] = new Picture(x, y, GameConstants.GRAY_HEART_IMAGE);
            grayHeart[i].draw();

            blackHeart[i] = new Picture(x, y, GameConstants.BLACK_HEART_IMAGE);
            blackHeart[i].draw();

            x += GameConstants.X_DISTANCE_HEARTS;
        }
    }

    private void createGoal() {

        goal = GameObjectFactory.createGoal();
    }

    public void checkFlies(){

        if (charlie.getY() <= GameConstants.GROUND_Y && charlie.getY() > GameConstants.END_GOAL_SECTION_Y) { // when frog eats flies, poison flies and golden fly

            for(Fly fly : flies){

                if(fly instanceof PoisonFly){
                    if (charlie.getX() == fly.getX() && charlie.getY() == fly.getY()) {

                        if (fly.isAlive()) {

                            fly.delete();
                            fly.die();
                            charlie.loseLife();
                            redHeart[charlie.getLives()].delete();
                        }

                        if (charlie.getLives() == 0) {

                            isStarted = false;

                            charlie.die();
                            gameOver();

                            while(isGameOver){
                                System.out.println("Press Space");
                            }

                            restart();
                            return;
                        }
                    }
                }else{
                    if (charlie.getX() == fly.getX() && charlie.getY() == fly.getY() && flyCounter > 0) {

                        if (fly.isAlive()) {

                            fly.die();

                            flyCounter--;
                        }

                        if (flyCounter == 0) {
                            createBonusFly();

                        }
                    }
                }

            }

            if (flyCounter == 0) {

                if (charlie.getX() == bonusFly.getX() && charlie.getY() == bonusFly.getY()) {
                    if (bonusFly.isAlive()) {
                        bonusFly.delete();
                        bonusFly.die();
                        bonusFlyEaten = true;
                        badFrog = new BadFrog();
                        createBlackHearts();
                    }
                }
            }
        }

    }

    private void checkPos() {

        if(bonusFlyEaten){
            if(charlie.getKnifeY() == badFrog.getFrogY() && charlie.getKnifeX() == badFrog.getFrogX() && badFrog.isAlive()){

                badFrog.loseLife();
                blackHeart[badFrog.getLives()].delete();

                if(badFrog.getLives() == 0){

                    badFrog.die();
                }
            }
            if(charlie.getX() == badFrog.getX() && charlie.getY() == badFrog.getY() && badFrog.isAlive()){

                charlie.loseLife();
                redHeart[charlie.getLives()].delete();

                if (charlie.getLives() == 0) {

                    isStarted = false;

                    charlie.die();
                    gameOver();

                    while(isGameOver){
                        System.out.println("Press Space");
                    }

                    restart();
                }

            }
        }
        if (charlie.getY() < GameConstants.BEGIN_CROC_SECTION && (charlie.getX() < GameConstants.BEGIN_GOAL_SECTION_X  || charlie.getX() > GameConstants.END_GOAL_SECTION_X)) { // when frog falls in river

            isStarted = false;

            charlie.die();
            gameOver();

            while(isGameOver){

                System.out.println("Press Space");
            }

            restart();
            return;
        }


        if (charlie.getY() < GameConstants.END_GOAL_SECTION_Y && charlie.getX() > GameConstants.BEGIN_GOAL_SECTION_X  && charlie.getX() < GameConstants.END_GOAL_SECTION_X) { // charlie arrives at she-frog

            if (flyCounter > 0 || bonusFly.isAlive()) {

                goal.noJoy();
                return;
            }

            isStarted = false;

            charlie.happyEnding();

            gameOver();

            while(isGameOver){

                System.out.println("Press Space");
            }

            restart();

        }
    }

    private void createWasp() {

        int x = GameConstants.PADDING;
        int y = GameConstants.WASP_Y_POS;
        wasps = new Wasp[4];
        for(int i = 0; i < wasps.length/2; i++){

            wasps[i] = GameObjectFactory.createWasp(x, y, Direction.RIGHT, GameConstants.WASP_IMAGE);
            y += GameConstants.WASP_Y_NEXT_POS;

        }
        x = GameConstants.WASP_X_SEC_POS;
        y = GameConstants.WASP_Y_SEC_POS;
        for(int i = wasps.length/2; i < wasps.length; i++){

            wasps[i] = GameObjectFactory.createWasp(x, y, Direction.LEFT, GameConstants.WASP_LEFT_IMAGE);
            y += GameConstants.WASP_Y_NEXT_POS;

        }
    }

    private void moveAllWasp(){

        for (Wasp wasp : wasps){

            wasp.fly();

            if (charlie.getX() == wasp.getX() && charlie.getY() == wasp.getY()) {

                charlie.loseLife();
                wasp.waspSound();
                redHeart[charlie.getLives()].delete();

                if (charlie.getLives() == 0) {

                    isStarted = false;

                    charlie.die();
                    gameOver();

                    while(isGameOver){
                        System.out.println("Press Space");
                    }

                    restart();
                    break;
                }
            }
        }
    }

    private void createBonusFly() {

        if (flyCounter == 0) {

            int random = (int) (Math.random() * lillyPads.length);

            int x = lillyPads[random].getX();
            int y = GameConstants.GROUND_Y;

            bonusFly = GameObjectFactory.createBonusFly(x, y);
        }
    }

    private void gameOver() {

        if (!charlie.isAlive()) {

            gameOver = new Picture(GameConstants.PADDING, GameConstants.PADDING, GameConstants.GAME_OVER_IMAGE);
            gameOver.draw();

            Sound sound = new Sound(GameConstants.DEATH_SOUND);
            sound.play(true);
        }

        if (charlie.isHappy()) {

            happyEnding = new Picture(GameConstants.PADDING, GameConstants.PADDING, GameConstants.GAME_WIN_IMAGE);
            happyEnding.draw();
        }

        isGameOver = true;
    }

    private void restart() {

        if (charlie.isHappy()) {

            happyEnding.delete();
        }

        if(bonusFlyEaten){
            bonusFlyEaten = false;
            badFrog.delete();
            badFrog.spitDelete();
            charlie.knifeDelete();
        }

        for (LillyPad lillyPad : lillyPads) {
            lillyPad.delete();
        }

        for (Fly fly : flies) {

            if (fly != null) {

                fly.delete();
            }
        }

        for(Wasp wasp : wasps){
            wasp.delete();
        }

        try{
            start();
        }catch (InterruptedException iEx){
            System.out.println(iEx.getMessage());
        }
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE && !isStarted) {

            isGameOver = false;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_A && bonusFlyEaten){

            charlie.knife();
        }

        if (isStarted) {

            switch (keyboardEvent.getKey()) {

                case KeyboardEvent.KEY_LEFT:
                    charlie.setDirection(Direction.LEFT);
                    charlie.hop();
                    goal.deleteMessage();
                    break;

                case KeyboardEvent.KEY_RIGHT:
                    charlie.setDirection(Direction.RIGHT);
                    charlie.hop();
                    goal.deleteMessage();
                    break;

                case KeyboardEvent.KEY_UP:
                    charlie.setDirection(Direction.UP);
                    charlie.hop();
                    break;

                case KeyboardEvent.KEY_DOWN:
                    charlie.setDirection(Direction.DOWN);
                    charlie.hop();
                    goal.deleteMessage();
                    break;
            }
        }
    }

    @Override
    public void keyReleased(KeyboardEvent event) {
    }
}
